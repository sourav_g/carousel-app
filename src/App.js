import './style/App.css';
import Carousel from './components/Carousel';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Carousel/>
      </header>
    </div>
  );
}

export default App;
