/* eslint-disable jsx-a11y/anchor-is-valid */
import '../style/Carousel.css';
import {useEffect, useState} from 'react';
import {data} from '../data/carousel'
import Filter from './Filter';
const Carousel = (props) => {
    let [active, setActive] = useState(1);
    let [carouselItems, setCarouselItems] = useState([]);
    let [options, setOptions] = useState([]);
    let [selectedOption, setSelectedOption] = useState("laptop");
    let [carouselData, setCarouselData] = useState([]);
    let [animation, setAnimation] = useState(0);
    let [animationRight, setAnimationRight] = useState(0);
    const handleChange = (e) => {
        setSelectedOption(e.target.value);
    }
    const filterValues = () => {
       return data.map(option => option.category).filter((option,index,options) => {
            return options.indexOf(option) === index;
        });
    }
    const showNext = () => {
        if(active===carouselData.length-1){
            setActive(0);
        } else {
            setActive(active+1);
        }
        setAnimation(1);
        setTimeout(() => {
            setAnimation(0); 
        }, 500);
    }
    const showPrev = () => {
        if(active===0){
            setActive(carouselData.length-1);
        } else {
            setActive(active-1);
        }
        setAnimationRight(1);
        setTimeout(() => {
            setAnimationRight(0); 
        }, 500);
    }
    const getCarouselData = (selectedOption) => {
        return data.filter(val => val.category === selectedOption)
    }
    const showCarouselItem = (active,data) => {
        setCarouselData(data);
        let dataLength = data.length;
        if(dataLength > 0){
            let items = [];
            if(active-1<0){
                items.push(data[dataLength-1]);
            }else {
                items.push(data[active-1]);
            }
            items.push(data[active]);
            if(active+1>dataLength-1){
                items.push(data[0]);
            }else {
                items.push(data[active+1])
            }
            return items;
        }
    }
    useEffect(() => {
        let options = filterValues();
        setOptions(options);
        setSelectedOption(selectedOption);
        let data = getCarouselData(selectedOption);
        let items = showCarouselItem(active,data);
        setCarouselItems(items);
    },[active,selectedOption]);
    return carouselItems.length>0 && (
        <div className="main">
        <Filter options={options} selectedValue={selectedOption} handleChange={handleChange}/>
        <div className="carousel">
            <a className="arrow" id="prev" onClick={showPrev}>
                &#10094;
            </a>
            <div className={animation? "carouselItem slideSmall" : animationRight ? "carouselItem slideBig": "carouselItem"}>
                <img src={`${process.env.PUBLIC_URL}/assets/${carouselItems[0].img}`} alt={carouselItems[0].name}/>
                {carouselItems[0].name}
             </div>
             <div className={animation? "carouselItemMiddle slideLeft" : animationRight ? "carouselItemMiddle slideLeft": "carouselItemMiddle"}>
                <img src={`${process.env.PUBLIC_URL}/assets/${carouselItems[1].img}`} alt={carouselItems[1].name}/>
                {carouselItems[1].name}
             </div>
             <div className={animation? "carouselItem slideBig" : animationRight ? "carouselItem slideSmall": "carouselItem"}>
                <img src={`${process.env.PUBLIC_URL}/assets/${carouselItems[2].img}`} alt={carouselItems[2].name}/>
                {carouselItems[2].name}
             </div>
            <a className="arrow" id="next" onClick={showNext}>
                &#10095;
            </a>
        </div>
        </div>
    )
}

export default Carousel;