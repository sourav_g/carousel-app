import '../style/Filter.css';
const Filter = (props) => {

    const renderOptions = () => {
        return (
        props.options.map(option => <option key={option} name={option}>{option}</option>)
        )
    }
    return (
        <>
            <div className="dropdown">
            <select id="category" name="category" value={props.selectedValue} onChange={props.handleChange}>
                {
                    renderOptions()
                }
            </select>
            </div>
        </>
    )
}

export default Filter;