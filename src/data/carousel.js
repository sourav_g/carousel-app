export const data = [
    {
        category: 'laptop',
        name: 'Acer',
        img: 'laptop_acer.jpg'
    },
    {
        category: 'laptop',
        name: 'Asus',
        img: 'laptop_asus.jpg'
    },
    {
        category: 'laptop',
        name: 'Dell',
        img: 'laptop_dell.jpg'
    },
    {
        category: 'laptop',
        name: 'Lenovo',
        img: 'laptop_lenovo.jpg'
    },
    {
        category: 'laptop',
        name: 'Apple',
        img: 'laptop_mac.jpg'
    },
    {
        category: 'mobile',
        name: 'Huawei',
        img: 'mobile_huawei.jpg'
    },
    {
        category: 'mobile',
        name: 'Iphone',
        img: 'mobile_iphone.jpg'
    },
    {
        category: 'mobile',
        name: 'Nokia',
        img: 'mobile_nokia.jpg'
    },
    {
        category: 'mobile',
        name: 'Oneplus',
        img: 'mobile_oneplus.jpg'
    },
    {
        category: 'mobile',
        name: 'Samsung',
        img: 'mobile_samsung.jpg'
    },
    {
        category: 'clothing',
        name: 'Jeans',
        img: 'clothing_jeans.jpg'
    },
    {
        category: 'clothing',
        name: 'Shirt',
        img: 'clothing_shirt.jpg'
    },
    {
        category: 'clothing',
        name: 'Suits',
        img: 'clothing_suits.jpg'
    },
    {
        category: 'clothing',
        name: 'Trousers',
        img: 'clothing_Trousers.jpg'
    },
    {
        category: 'clothing',
        name: 'Tshirt',
        img: 'clothing_tshirt.jpg'
    },
];